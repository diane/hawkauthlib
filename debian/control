Source: hawkauthlib
Maintainer: Diane Trout <diane@ghic.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-requests <!nocheck>,
               python3-setuptools,
               python3-webob,
               python3-pytest <!nocheck>,
Standards-Version: 4.6.1
Homepage: https://github.com/mozilla-services/hawkauthlib

Package: python3-hawkauthlib
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Hawk Access Authentication protocol
 This is a low-level library for implementing Hawk Access Authentication, a
 simple HTTP request-signing scheme described in:
 .
     https://npmjs.org/package/hawk
 .
 To access resources using Hawk Access Authentication, the client must have
 obtained a set of Hawk credentials including an id and a secret key.  They use
 these credentials to make signed requests to the server.
